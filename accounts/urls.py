from django.urls import path
from accounts.views import signup, user_login,use_logout

urlpatterns = [
    path("signup/", signup, name= "signup"),
    path("login/", user_login, name= "user_login"),
    path("logout/", use_logout, name= "use_logout")

]